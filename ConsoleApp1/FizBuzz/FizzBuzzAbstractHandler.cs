﻿namespace ConsoleApp1.FizBuzz
{
    public abstract class FizzBuzzAbstractHandler
    {
        private FizzBuzzAbstractHandler NextHandler = null;

        protected abstract string DetermineFizzBuzz(int value);

        public string Determine(int value)
        {
            var response = DetermineFizzBuzz(value);

            if (string.IsNullOrEmpty(response) && NextHandler != null)
            {
                return NextHandler.Determine(value);
            }
            else
            {
                if (!string.IsNullOrEmpty(response))
                {
                    return response;
                }
                
                if (NextHandler == null)
                {
                    response = value.ToString();
                }
            }

            return response;
        }

        public void SetNext(FizzBuzzAbstractHandler nextHandler)
        {
            this.NextHandler = nextHandler;
        }
    }
}