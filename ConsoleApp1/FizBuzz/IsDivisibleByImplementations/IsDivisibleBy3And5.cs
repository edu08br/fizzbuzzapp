﻿namespace ConsoleApp1.FizBuzz.IsDivisibleByImplementations
{
    public class IsDivisibleBy3And5 : FizzBuzzAbstractHandler
    {
        protected override string DetermineFizzBuzz(int value)
        {
            Math.DivRem(value, 3, out int divisibleBy3);
            Math.DivRem(value, 5, out int divisibleBy5);

            if ((divisibleBy3 == 0) && (divisibleBy5 == 0))
            {
                return "FizzBuzz";
            }

            return String.Empty;
        }
    }
}