﻿namespace ConsoleApp1.FizBuzz.IsDivisibleByImplementations
{
    public class IsDivisibleBy7 : FizzBuzzAbstractHandler
    {
        protected override string DetermineFizzBuzz(int value)
        {
            Math.DivRem(value, 7, out int divisibleBy7);

            if ((divisibleBy7 == 0))
            {
                return "Eduardo";
            }

            return String.Empty;
        }
    }
}