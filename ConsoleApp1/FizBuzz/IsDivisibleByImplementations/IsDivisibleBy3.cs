﻿namespace ConsoleApp1.FizBuzz.IsDivisibleByImplementations
{
    public class IsDivisibleBy3 : FizzBuzzAbstractHandler
    {
        protected override string DetermineFizzBuzz(int value)
        {
            Math.DivRem(value, 3, out int divisibleBy3);

            if ((divisibleBy3 == 0))
            {
                return "Fizz";
            }

            return String.Empty;
        }
    }
}