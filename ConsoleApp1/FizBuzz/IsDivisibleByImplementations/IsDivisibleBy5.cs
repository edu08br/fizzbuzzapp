﻿namespace ConsoleApp1.FizBuzz.IsDivisibleByImplementations
{
    public class IsDivisibleBy5 : FizzBuzzAbstractHandler
    {
        protected override string DetermineFizzBuzz(int value)
        {
            Math.DivRem(value, 5, out int divisibleBy5);

            if ((divisibleBy5 == 0))
            {
                return "Buzz";
            }

            return String.Empty;
        }
    }
}