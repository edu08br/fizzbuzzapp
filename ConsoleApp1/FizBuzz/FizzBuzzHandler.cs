﻿using ConsoleApp1.FizBuzz.IsDivisibleByImplementations;

namespace ConsoleApp1.FizBuzz
{
    public class FizzBuzzHandler
    {
        private IsDivisibleBy3And5 _isDivisibleBy3And5 = new();

        public FizzBuzzHandler()
        {
            IsDivisibleBy3 _isDivisibleBy3 = new();
            IsDivisibleBy7 _isDivisibleBy7 = new();
            IsDivisibleBy5 _isDivisibleBy5 = new();

            _isDivisibleBy3And5.SetNext(_isDivisibleBy3);
            _isDivisibleBy3.SetNext(_isDivisibleBy7);
            _isDivisibleBy7.SetNext(_isDivisibleBy5);
        }

        public string CheckIsDivisible(int index)
        {
            return _isDivisibleBy3And5.Determine(index);
        }
    }
}