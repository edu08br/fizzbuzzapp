﻿// See https://aka.ms/new-console-template for more information
using ConsoleApp1.FizBuzz;

FizzBuzzHandler fizzBuzzHandler = new();

for (var index = 1; index < 100; index++)
{
    Console.WriteLine(fizzBuzzHandler.CheckIsDivisible(index));
}

Console.ReadKey();